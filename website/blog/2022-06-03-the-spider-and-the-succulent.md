---
title: The Spider and the Succulent
author: Samson Szakacsy
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

There was once a spider who was falling behind while learning to spin a web. All of his friends and family learned so quickly, but he just tried and tried to no avail. Just has the spider was ready to give up, it noticed a succulent nearby. The spider spun its web to match the shape of the succulent and finally did it! The two were good friends for a long time.
