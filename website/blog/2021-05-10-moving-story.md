---
title: Moving To a New Place
author: Lea Tuizat
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Boxes boxes everywhere,
Packing away my things with care.

Bringing them with me to a new place,
So that I can resume life with grace.
