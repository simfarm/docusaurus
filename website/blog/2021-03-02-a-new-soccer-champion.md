---
title: A New Soccer Champion
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

There once was a boy from Uganda who dreamed of one day playing soccer professionally. His favorite teams were Manchester City and Chelsea. He dreamed of soccer every night, and spent hours working on refining his game at the pitch.

One day as the child grew older he met a wise mentor who told him about how difficult it was to become a soccer star. He instead convinced the child to go into sports media.

This boy did indeed live out his dream, the rest of his life as a sports media columnist.
