---
title: Newfoundland People and Dogs
author: Matthew Wyman
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

A Long time ago, in the artic cold of Canada a group of people just like you and me grew sad. They needed friends to help with the harsh work during the winter, pulling sleds, and carts and of course for excellent hugs and kisses. One Group said we need a big dogo for help his name was Gus. And Gus was the first Newfoundland dog. He did more than help. He became family!
