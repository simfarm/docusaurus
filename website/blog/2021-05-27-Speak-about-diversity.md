---
title: Speak about diversity
author: Tatiana Fernandez
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Promoting inclusiveness and diversity within the workplace is one of the best ways to foster an open-minded, global company culture. 
