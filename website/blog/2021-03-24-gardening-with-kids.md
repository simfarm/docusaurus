---
title: Gardening With Kids
author: Danielle Madry
authorURL: http://twitter.com/
authorFBID: 100002976521003
---
Gardening with Kids can be a great task for your children to do with you throughout the week. It teaches them responsibility and how to be caring. You can start with a small flower pot and some simple seeds. 

Here is a link to use to get started: https://www.etsy.com/market/kids_gardening_kits?utm_source=google&utm_medium=cpc&utm_campaign=Search_US_DSA_GGL_Categories_Home_New&utm_ag=Garden%252BSupplies&utm_custom1=_k_CjwKCAjwxuuCBhATEiwAIIIz0SbuBz1oYnjsUnjidjG8_CApazuOXIAGZBw_0EJvPxlCUtQqk4atXxoC0SYQAvD_BwE_k_&utm_content=go_1806416560_75040295688_473250799139_dsa-401187944775_c_&utm_custom2=1806416560&gclid=CjwKCAjwxuuCBhATEiwAIIIz0SbuBz1oYnjsUnjidjG8_CApazuOXIAGZBw_0EJvPxlCUtQqk4atXxoC0SYQAvD_BwE
