---
title: Going remote during pandemic
author: Bora Tasovac
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

The world pandemic that is ongoing for more than a year now made a lot of companies started to explore the work-from-home model.

Since remote teams require different tools and processes, that wasn't an easy change.

However, after a lot of months of constant struggle, many companies realized that this model can actually be more productive and will continue to work from home even after the pandemic is done. 
