---
title: Changing Seasons
author: Lukas Mertensmeyer
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

A change of seasons very much affects the weather. With the current clothing, it could quickly become too hot or too cold. Therefore, it is important to think about the right clothes already several weeks before the change of seasons.
