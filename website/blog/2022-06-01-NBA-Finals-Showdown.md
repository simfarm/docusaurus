---
title: NBA Finals Showdown
author: Zach Cole
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Each year 30 NBA teams work and fight for the chance to enter the coveted final series. Only two are left and their televised head-to-head contest will have viewers on the edge of their seats. Are the Warriors a worthy favourite? Only time will tell.
