---
title: The Slippery Water Slide
author: Kelsey Bartlett
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Sliding down the slippery slide faster and faster the children sped. The slide twisted, turned, looped around and even went upside down. And of course it ended with a big old SPLASH!
