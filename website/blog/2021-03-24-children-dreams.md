---
title: A Child's first Dreams
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

There once was a boy named Thomas. He grew up with a great imagination and his days were full of joy. He had wonderful parents and lived in a great community.

In the future bad things happened in Thomas's life. How can Thomas start to dream again?
