---
title: GitHub The Social Platform
author: Christopher Wang
authorURL: http://twitter.com/ericnakagawa
authorFBID: 661277173
---

GitHub prides itself on being a social platform for developers. The story is that it one day combined a community aspect along with the coding aspect. The challenge was that all of the open source communities ended up on GitHub.

What happened next? GitHub and GitLab ended up splitting the market. The end.
