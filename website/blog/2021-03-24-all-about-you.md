
---
title: All About You
author: Alex Sandoval
authorURL: http://twitter.com/
---

Nicole did not know how to say no, it was always hard for her to put herself first and people took advantage of that.

It made it very difficult for her emotionally. She learned that thinking about yourself was not putting everyone else aside, after all, how can you take care of someone else if you do not take care of yourself first?
