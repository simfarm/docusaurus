---
title: Best FPS games for children
author: Dorde Sumenkovic
authorURL: http://twitter.com/
authorFBID: 100002976521003
---


Everyone loves to play some video games. It could be PC, mobile or console ones. 

However there will always be a discussion about if a game has appropriate content for children. 

It depends. Here we'll describe where to go depending on genre.

1. Fortnite - it's a creative sandbox shooter where your kid can enjoy friend's company and open his imagination in the new world of Fortnite by bulding his way to be the #1 on the map.

2. Crash Bandicoot Racer - for fast youngsters loving the adrenaline.
